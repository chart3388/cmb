set(CTEST_USE_LAUNCHERS "ON" CACHE STRING "")

set(cmb_enable_testing "ON" CACHE BOOL "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_sccache.cmake")

# Include the superbuild settings.
include("$ENV{SUPERBUILD_PREFIX}/cmb-developer-config.cmake")
