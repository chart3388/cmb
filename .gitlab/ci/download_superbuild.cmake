cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  # 20201001
  set(file_item "5f75d22350a41e3d19beed33")
  set(file_hash "a555ef360539ba7c8fd838b9c94e670a26b292bcad16417423ae5cfe54ac8efaf9be2995ef3775ebdc6007c56568a5c539d6c5acaf2d6ff50d8d496b6af7ee81")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  # 20200912
  set(file_item "5f74716e50a41e3d19bb5ef4")
  set(file_hash "26832c678d2c41f247005c0d56736933cbf12a3fed8db9449def49dbce598c8f332f070c2932ceeefeface273e32a4b323a578408fe13c9a64361a14e96d0101")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
