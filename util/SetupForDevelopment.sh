#!/usr/bin/env bash

cd "${BASH_SOURCE%/*}/.." &&
util/GitSetup/setup-user && echo &&
util/GitSetup/setup-hooks && echo &&
util/GitSetup/setup-lfs && echo &&
util/SetupGitAliases.sh && echo &&
(util/GitSetup/setup-upstream ||
 echo 'Failed to setup origin.  Run this again to retry.') && echo &&
(util/GitSetup/setup-gitlab ||
 echo 'Failed to setup GitLab.  Run this again to retry.') && echo &&
util/GitSetup/tips

echo "Initializing and updating git submodules..."
git submodule update --init --recursive

# Rebase master by default
git config rebase.stat true
git config branch.master.rebase true

# Record the version of this setup so Scripts/pre-commit can check it.
SetupForDevelopment_VERSION=3
git config hooks.SetupForDevelopment ${SetupForDevelopment_VERSION}

echo "Setting up SMTK's development environment..."
thirdparty/smtk/utilities/SetupForDevelopment.sh
