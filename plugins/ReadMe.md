# Plugins Directory

Plugins that are specific to modelbuilder (as opposed to those
which may also be used in ParaView) belong in this directory.
Plugins that may be used in ParaView and do not rely on modelbuilder
belong in SMTK in the `smtk/extension/paraview` directory.

+ `postprocessing-mode` — add a toolbar button that
    + enables/disables the "Sources" and "Filters" menu items;
    + shows/hides the ParaView PipelineBrowser; and
    + shows/hides the ParaView filter toolbars (for common and data analysis filters)
  in order to expose post-processing options in modelbuilder.
  This plugin is expected to be useful for "expert" users and can
  cause confusion for "novices," so it is not included as a base
  feature in modelbuilder.
