//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef modelbuilder_mbMenuBuilder_h
#define modelbuilder_mbMenuBuilder_h

class QMenu;
class QWidget;
class QMainWindow;

class mbMenuBuilder
{
public:
  /**\brief Build the File menu.
    *
    */
  static void buildFileMenu(QMenu& menu);
};

#endif
